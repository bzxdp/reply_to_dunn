#Ctenophora-sister is a tree reconstruction artifact.#
##A Response to [Dunn (2018)](https://github.com/caseywdunn/feuda_2017_response/blob/master/manuscript.md)##
 
---

**Davide Pisani 1,2§, Roberto Feuda 1, Martin Dohrmann 3, Walker Pett 4, Hervé Philippe 5,6, Omar Rota-Stabelli 7, Nicolas Lartillot 8, Gert Wörheide 3,9,10§**
 
1 School of Biological Sciences, University of Bristol, UK.

2 School of Earth Sciences, University of Bristol, UK. 

3 Department of Earth- and Environmental Sciences, Palaeontology & Geobiology, Ludwig-Maximilians-Universität München, Munich, Germany. 

4 Department of Ecology, Evolution, & Organismal Biology, Iowa State University, Ames, IA 50011, USA. 

5 Centre de Théorisation et de Modélisation de la Biodiversité, Station d’Ecologie Théorique et Expérimentale, UMR CNRS 5321, Moulis 09200, France.

6 Département de Biochimie, Centre Robert-Cedergren, Université de Montréal, Montréal, QC, Canada H3C 3J7. 

7 Research and Innovation Centre, Fondazione Edmund Mach (FEM), Via E. Mach 1, 38010, San Michele all’Adige (TN), Italy. 

8 Université Lyon 1, CNRS, UMR 5558, Laboratoire de Biométrie et Biologie Evolutive.

9 GeoBio-Center, Ludwig-Maximilians-Universität München, Munich, Germany. 

10 SNSB - Bayerische Staatssammlung für Paläontologie und Geologie, Munich, Germany. 


§Corresponding authors, emails: davide.pisani@bristol.ac.uk; woerheide@lmu.de

---

The relationships at the root of the Metazoa have proven difficult to resolve and two competing hypotheses, namely “Porifera-sister” (sponges as the sister of all other animals) and “Ctenophora-sister” (comb jellies as the sister instead) lately dominated the discussion [1]. Most recently, [2] performed analyses arguing that compositional heterogeneity in amino acid datasets might be a confounding factor in early animal phylogeny, and that Ctenophora-sister is most likely an artifact (see also [3]). Dunn [4] claimed that the conclusions of [2] do not follow from their results. He identified three potential problems: (1) that not all the analyses of [2] had posterior probability (PP) >0.95 for Porifera-sister; (2) that only one model (CAT-GTR+G) provides support to Porifera-sister; (3) that the data recodings used by [2] remove information rather than heterogeneity. We address each point individually.
 
(1) Support
Dunn [4] objects that [2] presented results of Posterior Predictive tests (PPA) only for one of their outgroup-subsampling schemes and that for that scheme only one CAT-GTR+G analysis strongly supported Porifera-sister (PP >0.95). However, this was because [2] conservatively calculated PPA scores only for the outgroup-subsampling providing the least support to Porifera-sister. Here, we present PPA scores for the remaining subsamplings (Table 1) showing that PPA scores are comparable across all schemes. We agree with Dunn [4] that some of the results in Table 3 of [2], particularly for the dataset of [5] (hereafter Chang) have PP <0.95. However, the pattern over Tables 1 and 3 of [2] and our Table 1 shows that combining data-recoding with CAT-GTR+G always generates better goodness-of-fit measures. This is invariably associated with a sharp decrease in the support for Ctenophora-sister – PP ≤0.29. Recoded analyses favour Porifera-sister (even if not always with PP >0.95), except two analyses for which PP <0.5 for both hypotheses (see supplementary material).  
 
(2) Porifera-sister emerges only under CAT-GTR+G 
Dunn [4] states that [2] rejected GTR+G analyses supporting Ctenophora-sister. Dunn view’s is clarified in [6] where he argues that “GTR+G and CAT-GTR+G are very similar models.” We disagree, CAT-GTR+G differs fundamentally from GTR+G in its use of a complex non-parametric mixture over profiles. The two models differ profoundly in terms of goodness-of-fit ([2,7] and Table 1), and our rejection of the GTR+G analyses was based on objective model selection procedures grounded in well-corroborated observations indicating that better-fitting models are more likely to infer accurate trees (e.g. [8,9]). Considering trees obtained using GTR+G would be inconsistent with current Bayesian and decision-theoretic approaches to model selection [10]. Accordingly, given the overwhelming better empirical fit of CAT-GTR+G in all our analyses, we reiterate that support for Ctenophora-sister under GTR+G is of little relevance.
 
(3) Data Recoding
Based on an experiment showing a systematic tendency of CAT-GTR+G to favour Porifera-sister (for dataset D20 [11] - hereafter D20) when using recoding strategies that randomly bin amino acids into arbitrary categories, [4] argues that the impact of recoding is largely caused by loss of phylogenetic signal.  While random recoding is not novel [12], the experiment of [4] is insightful. However, we disagree with Dunn’s interpretation. 
Dunn’s [4] uses only D20 to show that random recoding recovers Porifera-sister. However, CAT-GTR+G analyses of randomly recoded versions of Chang strongly support Ctenophora-sister instead (Table 2). This is in stark contrast to the application of biologically-informed recodings (supplementary material), which never support Ctenophora-sister (Table 2 vs. Table 3 of [2]). 
We agree with [4], see also [2], that recoding removes some (not necessarily phylogenetic) information by masking substitutions. This loss can be quantified comparing the length of trees (TL – expressed as the mean number of substitutions per site) from recoded and non-recoded data. Tree lengths were about 28% shorter for random recodings of D20 (TL ≈25 vs. TL ≈37 non-recoded), or about 59% shorter when recoded using S&R6 [2,12] (TL ≈15).  This is equivalent to masking approximately 28% or 59% of all substitutions.  Yet, this loss of information does not explain the results of [2]. This can be verified using a jackknife procedure to emulate the information loss incurred when recoding. To test whether simply reducing the number of identifiable substitutions has an impact on the inferred tree, we randomly deleted, respectively, 28% and 59% of the sites from D20. CAT-GTR+G analyses of the shortened, amino acid, alignments (supplementary material), recover Ctenophora-sister (PP >0.95 in all but one case – Table 3). This stands in sharp contrast with what obtained when applying biologically-informed recodings [2], where Ctenophora-sister is never recovered. These observations illustrate that a loss of information per se cannot cause the shift towards Porifera-sister observed by [2]. 
 Recoding seems to do more than just remove information, and there are different ways by which it can positively impact phylogenetic reconstruction. First, by binning states that will typically not realize their most extreme frequencies for the same positions or the same species, even random recodings may be expected to cause a statistical averaging-out of across-site and -taxon heterogeneity.  Second, the sheer reduction of the size of the state space, from 20 down to 6, causes a simplification of the original inference problem (in particular the estimation of site-specific state preferences) into a lower-dimensional one, making it easier to solve using current methods. 

In conclusion, the results of [4] do not overturn [2]. However, we acknowledge that the randomization experiment of [4] suggests that the statistical properties of recodings need further investigations. More studies are necessary, both theoretical and empirical, particularly using larger datasets (e.g. [3]) that will better compensate for the loss of information inherent to data recoding. We also agree with [4] (see [2]) that the efficient implementation of models that can accommodate lineage- and site-specific compositional heterogeneity (e.g. [13]) is of paramount importance.  However, we note that recoding would be expected to improve goodness of fit also for these models – as it does for CAT-GTR+G. 

Our goodness-of-fit analyses, combined with previous analyses and observations (including support for Porifera-sister from the recoded datasets of [3] and [14] – in [15]), further strengthen the case against the use of site-homogeneous models in deep-time phylogenetics. Our results suggest that support for Ctenophora-sister invariably obtained using site-homogeneous models as well as using CAT-GTR+G for several amino acid datasets, represents a reconstruction artifact.

**References**


1. King, N., and Rokas, A. (2017). Embracing Uncertainty in Reconstructing Early Animal Evolution. Curr. Biol. 27, R1081–R1088.
2.	Feuda, R., Dohrmann, M., Pett, W., Philippe, H., Rota-Stabelli, O., Lartillot, N., Wörheide, G., and Pisani, D. (2017). Improved Modeling of Compositional Heterogeneity Supports Sponges as Sister to All Other Animals. Curr. Biol. 27, 3864–3870.e4.
3.	Simion, P., Philippe, H., Baurain, D., Jager, M., Richter, D.J., Di Franco, A., Roure, B., Satoh, N., Quéinnec, É., Ereskovsky, A., et al. (2017). A Large and Consistent Phylogenomic Dataset Supports Sponges as the Sister Group to All Other Animals. Curr. Biol. Available at: http://dx.doi.org/10.1016/j.cub.2017.02.031.
4.	Dunn, C.W. (2018). Response to Feuda et al. Curr. Biol. https://github.com/caseywdunn/feuda_2017_response/blob/master/manuscript.md
5.	Chang, E.S., Neuhof, M., Rubinstein, N.D., Diamant, A., Philippe, H., Huchon, D., and Cartwright, P. (2015). Genomic insights into the evolutionary origin of Myxozoa within Cnidaria. Proceedings of the National Academy of Sciences 112, 14912–14917.
6.	https://github.com/caseywdunn/feuda_2017/blob/master/essay.md
7.	Pisani, D., Pett, W., Dohrmann, M., Feuda, R., Rota-Stabelli, O., Philippe, H., Lartillot, N., and Wörheide, G. (2015). Genomic data do not support comb jellies as the sister group to all other animals. Proc. Natl. Acad. Sci. U. S. A. 112, 15402–15407.
8.	Cunningham, C.W., Zhu, H., and Hillis, D.M. (1998). Best-Fit Maximum-Likelihood Models for Phylogenetic Inference: Empirical Tests with Known Phylogenies. Evolution 52, 978–987.
9.	Posada, D., and Buckley, T. (2004). Model Selection and Model Averaging in Phylogenetics: Advantages of Akaike Information Criterion and Bayesian Approaches Over Likelihood Ratio Tests. Syst. Biol. 53, 793–808.
10.	Berger, J.O. (2013). Statistical Decision Theory and Bayesian Analysis (Springer Science & Business Media).
11.	Whelan, N.V., Kocot, K.M., Moroz, L.L., and Halanych, K.M. (2015). Error, signal, and the placement of Ctenophora sister to all other animals. Proc. Natl. Acad. Sci. U. S. A. 112, 5773–5778.
12.	Susko, E., and Roger, A.J. (2007). On reduced amino acid alphabets for phylogenetic inference. Mol. Biol. Evol. 24, 2139–2150.
13.	Blanquart, S., and Lartillot, N. (2006). A Bayesian compound stochastic process for modeling nonstationary and nonhomogeneous sequence evolution. Mol. Biol. Evol. 23, 2058–2071.
14.	Cannon, J.T., Vellutini, B.C., Smith, J., 3rd, Ronquist, F., Jondelius, U., and Hejnol, A. (2016). Xenacoelomorpha is the sister group to Nephrozoa. Nature 530, 89–93.
15.	Eitel, M., Francis, W.R., Varoqueaux, F., Daraspe, J., Osigus, H.-J., Krebs, S., Vargas, S., Blum, H., Williams, G.A., Schierwater, B., et al. (2018). Comparative genomics and the nature of placozoan species. PLoS Biol. 16, e2005359.
16.	Dayhoff, M.O., Schwartz, R.M., and Orcutt, B.C. (1978). A model of evolutionary change in proteins. In Atlas of protein sequence and structure, M. O. Dayhoff, ed. (Silver Spring (MD): National Biomedical Research Foundation), pp. 345–352.
17.	Kosiol, C., Goldman, N., and Buttimore, N.H. (2004). A new criterion and method for amino acid classification. J. Theor. Biol. 228, 97–106.


---

**Table 1**: PPA results for D20 Holo and Choano (outgroup subsampling schemes – for Opistho subsampling scheme see Table 1 of [2] . As in [2] Dayhoff-6 is the six states recoding scheme of [16]; S&R6 is the six states recoding scheme of [12]; KGB6 is the six states recoding scheme of [17].


| PPA              | Model & Coding     | D20-Holo | D20-Choano |
| --------         | --------           | ------| -----|
| DIV (Z-score)    | AA: GTR+G	        | 94.76	| 76.48
|                  | AA: CAT-GTR+G      | 5.92	| 5.63
|                  | Dayhoff-6: GTR+G	|63.95	|62.43
|                  |Dayhoff-6: CAT-GTR+G| -0.71 |-0.48
|                  |S&R6: GTR+G	        |69.10	|60.94
|                  |S&R6: CAT-GTR+G     |-0.89  |-0.54
|                  |KGB6: GTR+G         | 45.77 |59.88
|                  |KGB6: CAT-GTR+G     |-0.21  |0.10
|MAX (Z-score)	   |AA:GTR+G	        |52.68  |57.51
|                  |AA:CAT-GTR+G	    |20.05	|19.94
|	               |Dayhoff-6: GTR+G	|40.92	|48.83
|	               |Dayhoff-6:CAT-GTR+G	|22.99	|26.52
|	               |S&R6: GTR+G	        |23.48	|27.55
|	               |S&R6: CAT-GTR+G	    |5.30	|5.73
|	               |KGB6: GTR+G	        |47.31	|51.24
|	               |KGB6:CAT-GTR+G	    |18.07	|22.45


---


**Table 2**: Results of the random recoding experiments for D20 and Chang (10 replicates). Note: posterior probabilities do not sum up to 1 as in some cases Porifera+Ctenophora was also recovered.

Dataset|Random Recoding Replicate|  Supported topology (PP)    | |
|------|--------------------|----|-----------------------|
|||Ctenophora-sister|Porifera-sister|
|Chang|1|1.00|0.00
||2|1.00|0.00
||3|1.00|0.00
||4|1.00|0.00
||5|1.00|0.00
||6|1.00|0.00
||7|1.00|0.00
||8|1.00|0.00
||9|1.00|0.00
||10|1.00|0.00
D20|1|0.03|0.90
||2|0.01|0.89
||3|0.01|0.59
||4|0.05|0.67
||5|0.00|0.99
||6|0.00|1.00
||7|0.04|0.83
||8|0.02|0.86
||9|0.00|1.00
||10|0.00|1.00

---

**Table 3**: Results of the jackknife experiments on the amino acid version of D20, including posterior predictive analyses. Note: for the jackknife analyses mimicking the signal loss associated with a random recoding, PPA were only performed for the jackknife replicate n.1, because of computational limitations.

|Percent signal removed|Ctenophora-sister (PP)|PPA-DIV|PPA-MAX|
|----------------------|----------------------|-------|-------|
59%|1.0|3.59|17.84
||1.0|4.56|18.05
||1.0|4.49|22.09
||1.0|5.63|17.64
28%|1.0|6.48|25.94
||1.0|N.A.|N.A.
||1.0|N.A.|N.A.
||0.84|N.A.|N.A.

---

##Datafiles of analyses carried out can be found in folders under source.##